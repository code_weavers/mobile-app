package com.bitsweaver.herbaltech.myapplication.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.widget.EditText;

import com.bitsweaver.herbaltech.myapplication.Fragments.InitConfigFragment;
import com.bitsweaver.herbaltech.myapplication.R;
import com.bitsweaver.herbaltech.myapplication.Util.StaticMethods;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.future.ResponseFuture;

import java.util.concurrent.ExecutionException;

public class InitConfig extends FragmentActivity implements InitConfigFragment.OnFragmentInteractionListener {
    int type;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.initconfig);

        Intent i = getIntent();
        type = i.getIntExtra("type", 0);
        if (type == 1) {
            InitConfigFragment initConfigFragment = InitConfigFragment.newInstance(R.layout.fragment_reset, 1);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, initConfigFragment);
            transaction.commit();
        } else if (type == 2) {
            InitConfigFragment initConfigFragment = InitConfigFragment.newInstance(R.layout.fragment_verify_num, 2);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, initConfigFragment);
            transaction.commit();
        } else {
            finish();
        }
    }

    @Override
    public void onFragmentInteraction() {
        if (type == 1) {
//            reset password
            String new_pass = ((EditText) findViewById(R.id.new_pass)).getText().toString();
            String confirm_pass = ((EditText) findViewById(R.id.confirm_new_pass)).getText().toString();
            if (!new_pass.equals(confirm_pass)) {
                Snackbar.make(findViewById(R.id.fragment_container), "Password don't much", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return;
            }

            pDialog.show();
            ResponseFuture<JsonObject> res = StaticMethods.resetRequest(getApplicationContext(), new_pass);
            res.setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (pDialog.isShowing())
                        pDialog.dismiss();

                    if (e != null) {
                        e.printStackTrace();
                        Snackbar.make(findViewById(R.id.fragment_container), "Check your internet connection and try again later", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    if (!result.get("result").getAsBoolean()) {
                        Snackbar.make(findViewById(R.id.fragment_container), result.get("msg").getAsString(), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    } else {
                        Snackbar.make(findViewById(R.id.fragment_container), "Password has been Reset", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        finish();
                    }
                }
            });


        } else {
            String code = ((EditText) findViewById(R.id.code)).getText().toString();
            if (code.length() < 5) {
                Snackbar.make(findViewById(R.id.fragment_container), "The code is not valid try again", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return;
            }

            pDialog.show();
//                JsonObject obj = StaticMethods.verifyNumPost(getApplicationContext(), code);
            ResponseFuture<JsonObject> res = StaticMethods.verifyNumPost(getApplicationContext(), code);
            res.setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (pDialog.isShowing())
                        pDialog.dismiss();

                    if (e != null) {
                        Snackbar.make(findViewById(R.id.fragment_container), "Check your internet connection and try again", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                    if (!result.get("result").getAsBoolean()) {
                        Snackbar.make(findViewById(R.id.fragment_container), result.get("msg").getAsString(), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        return;
                    }
                    startActivity(new Intent(getApplicationContext(), Dashboard.class));
                    finish();
                }
            });
        }
    }

    @Override
    public void onFragmentInteraction(int type) {
        if (type == 2) {
            try {
                Snackbar.make(findViewById(R.id.fragment_container), "Please wait sending verification code", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                JsonObject obj = StaticMethods.sendVerifyCode(getApplicationContext());

                InitConfigFragment initConfigFragment = InitConfigFragment.newInstance(R.layout.enter_verify_code, 3);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, initConfigFragment);
                transaction.commit();


            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
                Snackbar.make(findViewById(R.id.fragment_container), "Check your internet connection and try again", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

        }
    }
}
