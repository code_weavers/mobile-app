package com.bitsweaver.herbaltech.myapplication.Recycler;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bitsweaver.herbaltech.myapplication.R;
import com.bitsweaver.herbaltech.myapplication.Util.StaticMethods;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.future.ResponseFuture;

/**
 * Created by oteng on 5/24/17.
 */

public class ProductViewHolder extends RecyclerView.ViewHolder {

    private TextView product_name;
    private TextView product_price;
    private TextView product_quantity;
    private TextView product_available;
    private TextView product_status;
    private TextView product_date;
    private TextView confirm_notice;
    private View view;

    private String product;


    ProductViewHolder(View itemView) {
        super(itemView);
        this.view = itemView;

        product_name = (TextView) itemView.findViewById(R.id.product_name);
        product_price = (TextView) itemView.findViewById(R.id.product_price);
        product_quantity = (TextView) itemView.findViewById(R.id.product_quantity);
        product_available = (TextView) itemView.findViewById(R.id.product_available);
        product_status = (TextView) itemView.findViewById(R.id.product_status);
        product_date = (TextView) itemView.findViewById(R.id.product_date);
        confirm_notice = (TextView) itemView.findViewById(R.id.confirm_notice);
    }

    void setOnclickListner(final Context context, final int position, final Callback cb) {
        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle("Confirm: " + product)
                        .setMessage("Are you sure you want to confirm receipt of the product?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                String id = view.getTag().toString();
//                                    JsonObject res = StaticMethods.confirmProduct(context, id);
                                ResponseFuture<JsonObject> res = StaticMethods.confirmProduct(context, id);
                                res.setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        if (e != null) {
                                            e.printStackTrace();
                                            Snackbar.make(view, "Check your internet and try again", Snackbar.LENGTH_LONG)
                                                    .setAction("Action", null).show();
                                            return;
                                        }
                                        cb.notifyStatusChange(position);

                                        if (!result.get("result").getAsBoolean()) {
                                            Snackbar.make(view, result.get("msg").getAsString(), Snackbar.LENGTH_LONG)
                                                    .setAction("Action", null).show();
                                        }
                                    }
                                });

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }


    void setClickable(boolean status) {
        if (status) {
            confirm_notice.setVisibility(View.GONE);
            this.view.setEnabled(false);
        }
    }

    void setProduct_name(String product_name) {
        this.product_name.setText(product_name);
        this.product = product_name;
    }

    void setProduct_price(String product_price) {
        this.product_price.setText(product_price);
    }

    void setProduct_quantity(String product_quantity) {
        this.product_quantity.setText(product_quantity);
    }

    void setProduct_available(String product_available) {
        this.product_available.setText(product_available);
    }

    void setProduct_status(String product_status) {
        this.product_status.setText(product_status);
    }

    void setProduct_date(String product_date) {
        this.product_date.setText(product_date);
    }

    void setTag(String tag) {
        this.view.setTag(tag);
    }

    public static interface Callback {
        void notifyStatusChange(int position);
    }
}
