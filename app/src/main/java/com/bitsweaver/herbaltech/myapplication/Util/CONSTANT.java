package com.bitsweaver.herbaltech.myapplication.Util;

/**
 * Created by oteng on 5/18/17.
 */

public class CONSTANT {
    public static String SETTINGS = "com_herbaltech_settings_file";

    //server parameters
    public static String USERNAME = "username";
    public static String PASSWORD = "password";
    public static String COOKIES = "cookies";

    //URL
    private static String BASE_URL_TEST_EMULATOR = "http://10.0.2.2:1337";
    private static String BASE_URL_TEST_PHONE = "http://192.168.43.164:3334";
    private static String BASE_URL_STAGE = "http://stage.herbaltechglobal.com";
    private static String BASE_URL_PRO = "http://herbaltechglobal.com";
    public static String BASE_URL = BASE_URL_TEST_PHONE;
    public static String LOGIN_URL = "/api2/login";
    public static String RESET_URL = "/api2/reset";
    public static String VERIFY_URL = "/api2/verify_number";
    public static String VERIFY_CODE = "/api2/send_verification_code";
    public static String PRODUCT_LIST = "/api2/get_product";
    public static String CONFIRM = "/api2/confirm";
    public static String DEDUCTIONFORMDATA = "/api2/get_deduction_formData";
    public static String URL_SALES = "/sales";
    public static String DIRECTSALEDATA = "/api2/get_sale_product";



}
