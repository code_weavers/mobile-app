package com.bitsweaver.herbaltech.myapplication.Recycler;

import android.net.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by oteng on 5/24/17.
 */

public class ProductModel {

    private String product_name;
    private String product_id;
    private String product_price;
    private String product_quantitu;
    private String product_status;
    private boolean status;
    private String product_avialable;
    private String product_date;


    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_quantitu() {
        return product_quantitu;
    }

    public void setProduct_quantitu(String product_quantitu) {
        this.product_quantitu = product_quantitu;
    }

    public String getProduct_status() {
        return product_status;
    }

    public void setProduct_status(boolean product_status) {
        this.status = product_status;
        this.product_status = product_status ? "confirmed" : "not confirmed";
    }

    public String getProduct_available() {
        return product_avialable;
    }

    public void setProduct_avialable(String product_avialable) {
        this.product_avialable = product_avialable;
    }

    public String getProduct_date() {
        return product_date;
    }

    public void setProduct_date(String product_date) {
        this.product_date = product_date;
    }

    public boolean getStatus() {
        return this.status;
    }

}
