package com.bitsweaver.herbaltech.myapplication.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.ImageView;

import com.bitsweaver.herbaltech.myapplication.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.future.ResponseFuture;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.BASE_URL;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.CONFIRM;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.DEDUCTIONFORMDATA;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.DIRECTSALEDATA;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.PRODUCT_LIST;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.RESET_URL;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.VERIFY_CODE;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.VERIFY_URL;

/**
 * Created by oteng on 5/18/17.
 */

public class StaticMethods {
    public static ResponseFuture<JsonObject> autoLogin(Context context) {
        return Ion.with(context)
                .load(BASE_URL + "/api2")
                .setHeader("Cookie", (SessionManager.getInstance(context.getApplicationContext())).getCookies())
                .asJsonObject();
    }

    public static void writeDataToSettings(Context context, Map<String, String> data) {
        SharedPreferences settings = context.getSharedPreferences(CONSTANT.SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        for (String key : data.keySet()) {
            editor.putString(key, data.get(key));
        }
        editor.apply();
    }

    public static ResponseFuture<JsonObject> resetRequest(Context context, String new_pass) {
        return Ion.with(context)
                .load(BASE_URL + RESET_URL)
                .setBodyParameter("new_pass", new_pass)
                .asJsonObject();
    }

    public static ResponseFuture<JsonObject> verifyNumGet(Context context) {
        return Ion.with(context)
                .load(BASE_URL + VERIFY_URL)
                .asJsonObject();
    }

    public static ResponseFuture<JsonObject> verifyNumPost(Context context, String code) {
        return Ion.with(context)
                .load(BASE_URL + VERIFY_URL)
                .setBodyParameter("verify_code", code)
                .asJsonObject();
    }

    public static JsonObject sendVerifyCode(Context context) throws ExecutionException, InterruptedException {
        return Ion.with(context)
                .load(BASE_URL + VERIFY_CODE)
                .asJsonObject()
                .get();
    }

    public static void loadProfilePic(ImageView imageView, String url) {
        Ion.with(imageView)
                .centerCrop()
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar_error)
                .load(BASE_URL + url);
    }

    public static ResponseFuture<JsonObject> getProductList(Context context) {
        return Ion.with(context)
                .load(BASE_URL + PRODUCT_LIST + "/" + UserDataManager.getInstance(context).getUserId())
                .asJsonObject();
    }

    public static ResponseFuture<JsonObject> confirmProduct(Context context, String product_id) {
        return Ion.with(context)
                .load(BASE_URL + CONFIRM + "/" + product_id)
                .asJsonObject();
    }

    public static ResponseFuture<JsonObject> getDeductionFormData(Context context) {
        return Ion.with(context)
                .load(BASE_URL + DEDUCTIONFORMDATA + "/" + UserDataManager.getInstance(context).getUserId())
                .asJsonObject();
    }

    public static ResponseFuture<JsonObject> getDirectSaleFormData(Context context) {
        return Ion.with(context)
                .load(BASE_URL + DIRECTSALEDATA + "/" + UserDataManager.getInstance(context).getUserId())
                .asJsonObject();
    }

}