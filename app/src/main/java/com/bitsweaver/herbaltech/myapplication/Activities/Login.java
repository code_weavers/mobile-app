package com.bitsweaver.herbaltech.myapplication.Activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bitsweaver.herbaltech.myapplication.R;
import com.bitsweaver.herbaltech.myapplication.Util.CONSTANT;
import com.bitsweaver.herbaltech.myapplication.Util.SessionManager;
import com.bitsweaver.herbaltech.myapplication.Util.StaticMethods;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.koushikdutta.ion.future.ResponseFuture;

import java.util.HashMap;
import java.util.Map;

import static com.bitsweaver.herbaltech.myapplication.R.id.btnLogin;

public class Login extends AppCompatActivity {
    private static final String TAG = Login.class.getSimpleName();

//    SharedPreferences sharedpreferences;

    private EditText username_editText;
    private EditText password_editText;
    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        username_editText = (EditText) findViewById(R.id.username);
        password_editText = (EditText) findViewById(R.id.password);
        findViewById(btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = username_editText.getText().toString().trim();
                String password = password_editText.getText().toString().trim();

                //TODO: show error on the view don't use tost
                // Check for empty data in the form
                if (!username.isEmpty() && !password.isEmpty()) {
                    // login user
                    doLogin(username_editText.getText().toString(), password_editText.getText().toString());

                } else {
                    // Prompt user to enter credentials
                    Snackbar.make(findViewById(R.id.login_main_view), "Please enter your credentials!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
//                    Toast.makeText(getApplicationContext(),
//                            "Please enter the credentials!", Toast.LENGTH_LONG)
//                            .show();
                }
            }
        });

//        log me in automatically if my session is till valid
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.show();

        ResponseFuture<JsonObject> res = StaticMethods.autoLogin(getApplicationContext());
        res.setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e != null) {
                    if (pDialog.isShowing())
                        pDialog.dismiss();
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Something went wrong check your internet connection and try again later", Toast.LENGTH_LONG).show();
                } else if ((result.get("result")).getAsBoolean()) {
                    if (pDialog.isShowing())
                        pDialog.dismiss();

                    startActivity(new Intent(getApplicationContext(), Dashboard.class));
                    finish();
                } else {
                    if (pDialog.isShowing())
                        pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Your session has expired, please log in again", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Function to log sales person in
     */
    private void doLogin(String username, String password) {
        pDialog.show();
        Ion.with(getApplicationContext()).
                load(CONSTANT.BASE_URL + CONSTANT.LOGIN_URL)
                .setBodyParameter("username", username)
                .setBodyParameter("password", password)
                .asJsonObject()
                .withResponse()
                .setCallback(new FutureCallback<Response<JsonObject>>() {
                    @Override
                    public void onCompleted(Exception e, Response<JsonObject> result) {
                        pDialog.dismiss();
                        if (e != null) {
                            e.printStackTrace();
                            Snackbar.make(findViewById(R.id.login_main_view), "Something went wrong check your internet connection and try again later", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            return;
                        }
//                        Log.d("Herbal", result.getResult())
                        JsonObject obj = result.getResult();
                        if (obj.get("result").getAsBoolean()) {
                            (SessionManager.getInstance(getApplicationContext())).setCookies(result.getHeaders().getHeaders().get("Set-Cookie"));
                            if (obj.get("reset_pass").getAsBoolean()) {
                                Intent i = new Intent(getApplicationContext(), InitConfig.class);
                                i.putExtra("type", 1);
                                startActivity(i);
                                return;
                            }
                            if (obj.get("verify_num").getAsBoolean()) {
                                //start reset password routine
//                                Log.d("Herbal", "reset");
                                Intent i = new Intent(getApplicationContext(), InitConfig.class);
                                i.putExtra("type", 2);
                                startActivity(i);
                                return;
                            }

                            Map<String, String> userData = new HashMap<>();
                            userData.put("userId", obj.get("userId").getAsString());
                            userData.put("name", obj.get("name").getAsString());
                            userData.put("pix", obj.get("pix").getAsString());

                            StaticMethods.writeDataToSettings(getApplicationContext(), userData);

                            startActivity(new Intent(getApplicationContext(), Dashboard.class));
                            finish();

                        } else {
                            Snackbar.make(findViewById(R.id.login_main_view), obj.get("msg").getAsString(), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
//                            Toast.makeText(getApplicationContext(), obj.get("msg").getAsString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


}
