package com.bitsweaver.herbaltech.myapplication.Recycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitsweaver.herbaltech.myapplication.Activities.Products;
import com.bitsweaver.herbaltech.myapplication.R;

import java.util.List;

/**
 * Created by oteng on 5/24/17.
 */

public class ProductAdaptor extends RecyclerView.Adapter<ProductViewHolder> {
    private List<ProductModel> productModel;
    private Context context;

    public ProductAdaptor(List<ProductModel> productModel, Context context) {
        this.productModel = productModel;
        this.context = context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_products, parent, false);
        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        ProductModel productModel = this.productModel.get(position);
        holder.setProduct_name(productModel.getProduct_name());
        holder.setProduct_available(productModel.getProduct_available());
        holder.setProduct_price(productModel.getProduct_price());
        holder.setProduct_date(productModel.getProduct_date());
        holder.setProduct_quantity(productModel.getProduct_quantitu());
        holder.setProduct_status(productModel.getProduct_status());
        holder.setTag(productModel.getProduct_id());
        holder.setOnclickListner(context, position, (ProductViewHolder.Callback) context);
        holder.setClickable(productModel.getStatus());

    }

    @Override
    public int getItemCount() {
        return this.productModel.size();
    }
}
