package com.bitsweaver.herbaltech.myapplication.Activities;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bitsweaver.herbaltech.myapplication.R;
import com.bitsweaver.herbaltech.myapplication.Util.StaticMethods;
import com.bitsweaver.herbaltech.myapplication.Util.UserDataManager;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static android.R.attr.value;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.BASE_URL;

public class Dashboard extends AppCompatActivity {
    String selecteditem;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_CROP = 2;
    File imageFile;
    private ImageView profilePic;
    private String customerPicPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo);

        UserDataManager userDataManager = UserDataManager.getInstance(getApplicationContext());
        ((TextView) findViewById(R.id.sellname)).setText(userDataManager.getUserName());
        StaticMethods.loadProfilePic((ImageView) findViewById(R.id.profile_pic), userDataManager.getProfilePix());

        Snackbar.make(findViewById(R.id.dashboard_main_view), ("Welcome " + userDataManager.getUserName()), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();


        findViewById(R.id.product_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Products.class));
            }
        });

        findViewById(R.id.sell_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);

                builder.setTitle("Choose an option");

                final String[] options = new String[]{
                        "Direct Cash",
                        "Deduction"
                };

                builder.setSingleChoiceItems(options, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                selecteditem = (String) options[i];
                                break;
                            case 1:
                                selecteditem = (String) options[i];
                                break;

                        }
                    }

                });

                builder.setPositiveButton("Next", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(selecteditem != null) {
                            if (selecteditem.equals("Direct Cash")) {
                                Intent myIntent = new Intent(Dashboard.this, Sell_direct.class);
                                myIntent.putExtra("key", value); //Optional parameters
                                Dashboard.this.startActivity(myIntent);
                            } else if (selecteditem.equals("Deduction")) {
                                Intent myIntent = new Intent(Dashboard.this, Sell_deduction.class);
                                myIntent.putExtra("key", value); //Optional parameters
                                Dashboard.this.startActivity(myIntent);
                            }
                        }
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog dialog = builder.create();

                dialog.show();

            }


        });

        findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Ion.with(getApplicationContext())
                            .load(BASE_URL + "/api2/logout")
                            .asJsonObject()
                            .get();
                    startActivity(new Intent(getApplicationContext(), Login.class));
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });

        profilePic = (ImageView) findViewById(R.id.profile_pic);
//        findViewById(R.id.profile_pic).setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                takePic();
//            }
//        });
    }

//    private void takePic() {
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//            File photoFile = null;
//            try {
//                photoFile = createImageFile();
//            } catch (IOException ex) {
//                Toast.makeText(getApplicationContext(), "Something went wrong can't take pictures", Toast.LENGTH_SHORT).show();
//            }
//            if (photoFile != null) {
//                Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
//                        "com.bitsweaver.herbaltech.myapplication.fileprovider",
//                        photoFile);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//            }
//
//        } else
//            Toast.makeText(getApplicationContext(), "Check your camera and try again", Toast.LENGTH_SHORT).show();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            performCrop();
            displayImage();


        }
        if (requestCode == REQUEST_IMAGE_CROP && resultCode == RESULT_OK) {
            //get the returned data
            Bundle extras = data.getExtras();
            //get the cropped bitmap
            Bitmap croppedPic = extras.getParcelable("data");
            //retrieve a reference to the ImageView
            ImageView cropPicView = (ImageView)findViewById(R.id.profile_pic);
            //display the returned cropped image
            cropPicView.setImageBitmap(croppedPic);
           // displayImage();

        }
    }

       private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

    }

    private void performCrop() {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            File f = new File(imageFile.getAbsolutePath());
            Uri contentUri = Uri.fromFile(f);
            cropIntent.setDataAndType(contentUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, REQUEST_IMAGE_CROP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void displayImage() {
        // Get the dimensions of the View
        int targetW = 1;
        int targetH = 1;
        targetW = profilePic.getWidth();
        targetH = profilePic.getHeight();
        //customerPicPath = imageFile.getAbsolutePath();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(customerPicPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(customerPicPath, bmOptions);
        profilePic.setImageBitmap(bitmap);
    }

}
