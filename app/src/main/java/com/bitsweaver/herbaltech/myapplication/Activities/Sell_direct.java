package com.bitsweaver.herbaltech.myapplication.Activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bitsweaver.herbaltech.myapplication.R;
import com.bitsweaver.herbaltech.myapplication.Util.StaticMethods;
import com.bitsweaver.herbaltech.myapplication.Util.UserDataManager;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.future.ResponseFuture;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.BASE_URL;
import static com.bitsweaver.herbaltech.myapplication.Util.CONSTANT.URL_SALES;

public class Sell_direct extends AppCompatActivity implements LocationListener{

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_CROP = 2;
    File imageFile;
    private ImageView customerPic;
    private String customerPicPath;

    private ProgressDialog pDialog;

    ArrayList<String> product;
    ArrayList<Float> price;
    ArrayList<Integer> product_id;

    Button getLocationBtn;
    TextView locationText;

    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        pDialog = new ProgressDialog(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_direct);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        customerPic = (ImageView) findViewById(R.id.customers_pic);

        getLocationBtn = (Button)findViewById(R.id.getLocationBtn);
        locationText = (TextView)findViewById(R.id.locationText);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        getLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });

        findViewById(R.id.photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePic();
            }
        });

        findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });

//        try {

        pDialog.setCancelable(false);
        pDialog.show();

//            JsonObject res = StaticMethods.getDirectSaleFormData(getApplicationContext());
        ResponseFuture<JsonObject> res = StaticMethods.getDirectSaleFormData(getApplicationContext());
        res.setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {

                if (pDialog.isShowing())
                    pDialog.dismiss();

                if(e != null){
                    Toast.makeText(getApplicationContext(), "Check your internet connection and try again later", Toast.LENGTH_SHORT).show();
                }

                if (result.get("result").getAsBoolean()) {
                    JsonArray pro_jarr = result.getAsJsonArray("data");

                    product = new ArrayList<>();
                    price = new ArrayList<>();
                    product_id = new ArrayList<>();

                    for (int i = 0; i < pro_jarr.size(); i++) {
                        JsonObject tm = pro_jarr.get(i).getAsJsonObject();
//                        if (tm.get("available").getAsInt() < 0) {
                            product.add(tm.get("name").getAsString());
                            price.add(tm.get("price").getAsFloat());
                            product_id.add(tm.get("product_id").getAsInt());
//                        }
                    }
                    ((Spinner) findViewById(R.id.productname)).setAdapter(new ArrayAdapter<>(getApplication(), R.layout.spinner_style, product));
                    ((Spinner) findViewById(R.id.productname)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ((EditText) findViewById(R.id.price)).setText(("" + price.get(position)));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

//                payment type array
                    final String[] options = new String[]{
                            "Mtn Money",
                            "Tigo Cash",
                            "Vodafone Cash",
                            "Airtel Money"
                    };
                    ((Spinner) findViewById(R.id.payment_type)).setAdapter(new ArrayAdapter<>(getApplication(), R.layout.spinner_style, options));


                } else {
                    Toast.makeText(getApplicationContext(), "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0));
        }catch(Exception e)
        {

        }

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getApplicationContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
       // Toast.makeText(activity_sell_direct.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }


    private void takePic() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Toast.makeText(getApplicationContext(), "Something went wrong can't take pictures", Toast.LENGTH_SHORT).show();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                        "com.bitsweaver.herbaltech.myapplication.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }

        } else
            Toast.makeText(getApplicationContext(), "Check your camera and try again", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            displayImage();
            performCrop();

        }
        if (requestCode == REQUEST_IMAGE_CROP && resultCode == RESULT_OK) {
            displayImage();

        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );

    }

    private void performCrop() {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(Uri.parse(imageFile.getAbsolutePath()), "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, REQUEST_IMAGE_CROP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void displayImage() {
        // Get the dimensions of the View
        int targetW = 1;
        int targetH = 1;
        targetW = customerPic.getWidth();
        targetH = customerPic.getHeight();
        customerPicPath = imageFile.getAbsolutePath();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(customerPicPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(customerPicPath, bmOptions);
        customerPic.setImageBitmap(bitmap);
    }

    // //Converting profile pic into base64
    public String encodeImageProfilePic(String filePath) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 8;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    private void send() {
        final String name = ((EditText) findViewById(R.id.name)).getText().toString();
        final String telephone = ((EditText) findViewById(R.id.telephone)).getText().toString();
        final String quantity = ((EditText) findViewById(R.id.quantity)).getText().toString();
        final String price = ((EditText) findViewById(R.id.price)).getText().toString();
        final String payment_type = ((Spinner) findViewById(R.id.payment_type)).getSelectedItem().toString();
        final String payment_id = ((EditText) findViewById(R.id.transaction_num)).getText().toString();
        final String amount_paid = ((EditText) findViewById(R.id.amount_paid)).getText().toString();
        final String locationText = ((TextView) findViewById(R.id.locationText)).getText().toString();

        if (name.length() < 2 || quantity.length() < 1 || payment_id.length() < 4 || amount_paid.length() < 1) {
            Toast.makeText(this, "All fields are required", Toast.LENGTH_SHORT).show();
            return;
        }
        if (telephone.length() < 10) {
            Toast.makeText(this, "Telephone number should be like this: 0245004504", Toast.LENGTH_SHORT).show();
            return;
        }

        if (locationText == null) {
            Toast.makeText(this, "Current Location is required", Toast.LENGTH_SHORT).show();
            return;
        }

        if (customerPicPath == null)
            Toast.makeText(this, "Customers Picture is required", Toast.LENGTH_SHORT).show();

        final String pro = ((Spinner) findViewById(R.id.productname)).getSelectedItem().toString();
        final int proPosition = ((Spinner) findViewById(R.id.productname)).getSelectedItemPosition();


        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Confirm Upload ")
                .setMessage("Are you sure all the data is correct and you want to upload?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        try {

                            pDialog.setCancelable(true);
                            pDialog.show();

                            JsonObject res = Ion.with(getApplicationContext())
                                    .load(BASE_URL + URL_SALES)
                                    .setBodyParameter("name", name)
                                    .setBodyParameter("product_id", ("" + product_id.get(proPosition)))
                                    .setBodyParameter("telephone", telephone)
                                    .setBodyParameter("profilepic", encodeImageProfilePic(customerPicPath))
                                    .setBodyParameter("userid", UserDataManager.getInstance(getApplicationContext()).getUserId())
                                    .setBodyParameter("quantity", quantity)
                                    .setBodyParameter("price", price)
                                    .setBodyParameter("payment_type", payment_type)
                                    .setBodyParameter("payment_ID", payment_id)
                                    .setBodyParameter("amount_paid", amount_paid)
                                    .setBodyParameter("locationText", locationText)
                                    .asJsonObject()
                                    .get();
//                            Log.d("Herbal", res.toString());

                            if (pDialog.isShowing())
                                pDialog.dismiss();

                            Toast.makeText(getApplicationContext(), "Transaction Submitted Please wait for approval SMS", Toast.LENGTH_SHORT).show();

                            //delete the picture once all is done
                            File fdelete = new File(customerPicPath);
                            if (fdelete.exists()) {
                                fdelete.delete();
                            }
                            finish();

                        } catch (InterruptedException | ExecutionException e) {
                            if (pDialog.isShowing())
                                pDialog.dismiss();
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
