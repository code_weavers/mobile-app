package com.bitsweaver.herbaltech.myapplication.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.bitsweaver.herbaltech.myapplication.R;
import com.bitsweaver.herbaltech.myapplication.Recycler.ProductAdaptor;
import com.bitsweaver.herbaltech.myapplication.Recycler.ProductModel;
import com.bitsweaver.herbaltech.myapplication.Recycler.ProductViewHolder;
import com.bitsweaver.herbaltech.myapplication.Util.StaticMethods;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.future.ResponseFuture;

import java.util.ArrayList;
import java.util.List;

public class Products extends AppCompatActivity implements ProductViewHolder.Callback {
    ProgressDialog pDialog;
    RecyclerView recList = null;
    List<ProductModel> productModel_list = new ArrayList<>();
    ProductAdaptor productAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.show();

        final Activity self = this;
        ResponseFuture<JsonObject> res = StaticMethods.getProductList(getApplicationContext());
        res.setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e != null) {
                    e.printStackTrace();

                    if (pDialog.isShowing())
                        pDialog.dismiss();

                    Snackbar.make(findViewById(R.id.product_main_page), "Something happened Please try again later", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    return;
                }

                if (pDialog.isShowing())
                    pDialog.dismiss();

                if (result.get("result").getAsBoolean()) {
                    JsonArray product_arr = result.getAsJsonArray("data");

                    for (JsonElement item : product_arr) {
                        JsonObject obj = item.getAsJsonObject();

                        ProductModel productModel = new ProductModel();
                        productModel.setProduct_name(obj.get("name").getAsString());
                        productModel.setProduct_price(obj.get("price").getAsString());
                        productModel.setProduct_quantitu(obj.get("quantity").getAsString());
                        productModel.setProduct_avialable(obj.get("available").getAsString());
                        productModel.setProduct_date(obj.get("date").getAsString());
                        productModel.setProduct_id(obj.get("id").getAsString());
                        productModel.setProduct_status(obj.get("confirm").getAsBoolean());

                        productModel_list.add(productModel);
                    }

                    Log.d("Herbal", "" + product_arr.size());

                    recList = ((RecyclerView) findViewById(R.id.products));
                    recList.setHasFixedSize(true);
                    LinearLayoutManager llm = new LinearLayoutManager(self);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recList.setLayoutManager(llm);
                    productAdaptor = new ProductAdaptor(productModel_list, self);
                    recList.setAdapter(productAdaptor);

                } else {
                    Snackbar.make(findViewById(R.id.product_main_page), "You don't have any product", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

    }

    @Override
    public void notifyStatusChange(int position) {
//        Log.d("Herbal", "" + position);
        ProductModel productModel = productModel_list.get(position);
        productModel.setProduct_status(true);
        productAdaptor.notifyDataSetChanged();
    }


}
