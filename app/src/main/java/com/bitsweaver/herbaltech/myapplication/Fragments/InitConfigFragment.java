package com.bitsweaver.herbaltech.myapplication.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bitsweaver.herbaltech.myapplication.R;
import com.bitsweaver.herbaltech.myapplication.Util.StaticMethods;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.future.ResponseFuture;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InitConfigFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InitConfigFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InitConfigFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_LAYOUT = "param1";
    private static final String ARG_TYPE = "param2";

    // TODO: Rename and change types of parameters
    private int layout;
    private int type;


    private OnFragmentInteractionListener mListener;

    public InitConfigFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param layout Parameter 1
     * @return A new instance of fragment InitConfigFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InitConfigFragment newInstance(int layout, int type) {
        InitConfigFragment fragment = new InitConfigFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT, layout);
        args.putInt(ARG_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            layout = getArguments().getInt(ARG_LAYOUT);
            type = getArguments().getInt(ARG_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final String[] num = {""};
        if (type == 2) {
            final ProgressDialog pDialog = new ProgressDialog(getContext());
            pDialog.setCancelable(false);
            pDialog.setTitle("Loading ...");
            pDialog.show();


            ResponseFuture<JsonObject> res = StaticMethods.verifyNumGet(getActivity().getApplicationContext());
            res.setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    Log.d("Herbal1", result.toString());
                    num[0] = result.get("num").getAsString();
                    pDialog.dismiss();
                }
            });


            View v = inflater.inflate(layout, container, false);
            v.findViewById(R.id.next_num).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    verify_code();
                }
            });
            ((TextView) v.findViewById(R.id.num)).setText(num[0]);
            return v;
        } else {
            View v = inflater.inflate(layout, container, false);
            v.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nextBtn();
                }
            });
            return v;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void nextBtn() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    public void verify_code() {
        if (mListener != null) {
            mListener.onFragmentInteraction(type);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();

        void onFragmentInteraction(int type);
    }
}
