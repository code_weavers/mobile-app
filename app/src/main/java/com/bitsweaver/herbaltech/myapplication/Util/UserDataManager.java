package com.bitsweaver.herbaltech.myapplication.Util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by oteng on 5/23/17.
 */

public class UserDataManager {

    private String userId;
    private String userName;
    private String profilePix;

    private UserDataManager(Context c) {
        SharedPreferences settings = c.getSharedPreferences(CONSTANT.SETTINGS, Context.MODE_PRIVATE);
        this.setUserId(settings.getString("userId", ""));
        this.setUserName(settings.getString("name", ""));
        this.setProfilePix(settings.getString("pix", ""));
    }

    private static UserDataManager userDataManager = null;

    public synchronized static UserDataManager getInstance(Context c) {
        if (userDataManager != null)
            return userDataManager;
        else {
            return new UserDataManager(c);
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfilePix() {
        return profilePix;
    }

    public void setProfilePix(String profilePix) {
        this.profilePix = profilePix;
    }
}
