package com.bitsweaver.herbaltech.myapplication.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by oteng on 5/18/17.
 */

public class SessionManager {
    private SharedPreferences settings;

    private SessionManager(Context c) {
        settings = c.getSharedPreferences(CONSTANT.SETTINGS, Context.MODE_PRIVATE);
    }

    private static SessionManager sessionManager = null;

    public synchronized static SessionManager getInstance(Context c) {
        if (sessionManager != null)
            return sessionManager;
        else {
            return new SessionManager(c);
        }
    }

    String getCookies(){
        String cookies =   this.settings.getString(CONSTANT.COOKIES, null);
        return cookies;
    }

    public void setCookies(String cookies){
        SharedPreferences.Editor e =  this.settings.edit();
        e.putString(CONSTANT.COOKIES, cookies);
        e.apply();
    }
}
