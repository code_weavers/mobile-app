1. app lunch:
    - try to log user in using stored cookie key
    - show login page when auto login fail

2. Login:
    - show error if fields not filled
    - show error if login on the server fields
    - show reset page when password needs reset
    - show verify page when number needs reset

3. Reset Page:
    - show reset page ( should have new password and confirm password input tags)
    - password entered in both fields should much
    - show error on server and stay on page
    - when all goes well show login page

4. Verify Phone Number:
    - show verify page with number
    - when next is clicked show the code page
    - send request to server to send code
    - when next clicked
    - if code is empty or not upto 5-6 show error and remain on page
    - send request to server
    - if all goes well show dashboard
    - show error if any thing happens
    - show verify page with number for user to start over
